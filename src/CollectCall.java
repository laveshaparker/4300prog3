import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
<<<<<<< HEAD
=======
import java.util.Collection;
>>>>>>> 4123e7ded942d6d23650f90d752f4d3930a5d3a8
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.util.Version;

<<<<<<< HEAD
/**
 * @author Alice: This is a program that takes in a direcotry argument and
 *         calculates na, and nab for the terms in the collection. It stores
 *         words as Shorts to reduce memory size. Once created the collection
 *         calculation can be saved to disc and then innitialized from disc
 *         (much faster). to do this, create collection with arg String: dir run
 *         method save() then create the collection without any arguments NOTE:
 *         this class cannot be modified after creation.
 */
public class CollectCall {
	private ThisAnalyzer sa = new ThisAnalyzer(Version.LUCENE_44, null, false,
			false);
	/**
	 * Treemap word-->Short Rep
	 */
	private TreeMap<String, Short> wordList;
	/**
	 * treemap Short rep-->word
	 */
	private TreeMap<Short, String> listWord;
	/**
	 * na occurence in collection, by Short rep of term
	 */
	private TreeMap<Short, Short> na;
	/**
	 * nab rep of collection, by Short rep of term A-->(tree map of Short rep of
	 * term B-->cooccurence count) If term1-->term2 is null check term2-->term1
	 * If both are null then terms do not co-occur
	 */
	private TreeMap<Short, TreeMap<Short, Short>> nab;
	/**
	 * Windows in collection (Docs in this case)
	 */
	private short N;

	/**
	 * Used to initialize collect call from a previous collect call saved to
	 * disc
	 * 
	 * @throws Exception
	 *             if an instance of collect call can't be initialized (not
	 *             found, or incomplete)
	 */
	public CollectCall() throws Exception {
		readFromDisc();

	}

	/**
	 * 
	 * @param dir
	 *            : String representation of directory
	 * @throws IOException
	 *             if Directory is not found, etc
	 */
	public CollectCall(String dir) throws IOException {
		build_na_nab(dir);
	}

	/**
	 * 
	 * @param text_dir
	 *            : Directory housing corpus, assumed to be a file with docs at
	 *            first level
	 * @throws IOException
	 *             if corpus is not found
	 */
	private void build_na_nab(String text_dir) throws IOException {

		wordList = new TreeMap<String, Short>();
		na = new TreeMap<Short, Short>();
		nab = new TreeMap<Short, TreeMap<Short, Short>>();

=======




/**
 * @author Alice:
 *This is a program that takes in a direcotry argument and calculates na, and nab for the terms in the collection. It stores
 *words as Integers to reduce memory size. Once created the collection calculation can be saved to disc and then innitialized
 *from disc (much faster).
 *to do this, create collection with arg String: dir
 *run method save()
 *then create the collection without any arguments
 *NOTE: this class cannot be modified after creation.
 */
public class CollectCall {
	private ThisAnalyzer sa = new ThisAnalyzer(Version.LUCENE_44, null, false, false);
	/**
	 * Treemap word-->Integer Rep
	 */
	private TreeMap<String, Integer> wordList;
	/**
	 * treemap Integer rep-->word
	 */
	private TreeMap<Integer, String> listWord;
	/**
	 * na occurence in collection, by Integer rep of term
	 */
	private TreeMap<Integer, Integer> na;
	/**
	 * nab rep of collection, by integer rep of term A-->(tree map of integer rep of term B-->cooccurence count)
	 * If  term1-->term2 is null check term2-->term1
	 * If both are null then terms do not co-occur
	 */
	private TreeMap<Integer, TreeMap<Integer, Integer>> nab;
	/**
	 * Windows in collection (Docs in this case)
	 */
	private int N;
	/**
	 * 
	 * @param dir: String representation of directory
	 * @throws IOException if Directory is not ound
	 */
	public CollectCall (String dir) throws IOException
	{
		build_na_nab(dir);
	}
	
	/**
	 * Used to initialize collect call from a previous collect call saved to disc
	 * @throws Exception if an instance of collect call can't be initialized (not found, or incomplete)
	 */
	public CollectCall () throws Exception
	{
			readFromDisc();
		
	}
	/**
	 * 
	 * @param text_dir: Directory housing corpus, assumed to be a file with docs at first level
	 * @throws IOException if corpus is not found
	 */
	private  void build_na_nab(String text_dir) throws IOException {
	
		wordList = new TreeMap<String, Integer>();
		na = new TreeMap<Integer, Integer>();
		nab = new TreeMap<Integer, TreeMap<Integer, Integer>>();
		
>>>>>>> 4123e7ded942d6d23650f90d752f4d3930a5d3a8
		File folder = new File(text_dir);
		File[] listOfFiles = folder.listFiles();
		String filename;
		System.out.println("Opening files...");
<<<<<<< HEAD
		short i = 0;
		short s = 0;
		System.out.println("Calculating corpus statistics...");
		for (File file : listOfFiles) { // Going through all of the files in the
										// directory denoted by text_dir
			if (file.isFile()) {// To avoid any runtime errors for missing files
				filename = file.getName();

				BufferedReader br = new BufferedReader(new FileReader(text_dir
						+ "/" + filename));
				Set<String> wordums = getWords(br);
				br.close();
				if (s % 64 == 0)
					System.out.println((s / 32) + "% completed");
				s++;
				Iterator<String> hereSumWords = wordums.iterator();
				String wordy;

				while (hereSumWords.hasNext()) {
					TreeMap<Short, Short> m = null;

					wordy = hereSumWords.next();
					Short p = wordList.get(wordy);
					if (p != null) {

						na.put(p, (short) (na.get(p) + 1)); // increase na
						m = nab.get(p);

					} else {

						wordList.put(wordy, ++i);
						na.put(i, (short) 1);
						m = new TreeMap<Short, Short>();
						p = i;

					}
					Iterator<String> coIt = wordums.iterator();

					// System.out.println(nab.get(4) + "index");
					while (coIt.hasNext()) {

						String hold = coIt.next();
						Short q = wordList.get(hold);

						if (!(q == null || q.equals(p))) {
							Short qp = nab.get(q).get(p);
							if (qp == null) {
=======
		int i = 0;
		int s = 0;
		System.out.println("Calculating corpus statistics...");
		for (File file : listOfFiles){ // Going through all of the files in the directory denoted by text_dir
			if (file.isFile()){//To avoid any runtime errors for missing files
				filename = file.getName();
				
				BufferedReader br = new BufferedReader(new FileReader(text_dir+"/"+filename));
	            Set<String> wordums = getWords(br);
					br.close();
				if (s % 64 == 0)
					System.out.println((s/32) + "% completed");
				s++;
				Iterator<String> hereSumWords = wordums.iterator();
				String wordy;
				
				
				while (hereSumWords.hasNext()){
					TreeMap<Integer, Integer> m = null;

					wordy = hereSumWords.next();
					Integer  p = wordList.get(wordy);
					if (p != null)
					{
						
						na.put(p, na.get(p) + 1); //increase na
						m = nab.get(p);
						
					}
					else
					{
						
						wordList.put(wordy, ++i);
						na.put(i, 1);
						m = new TreeMap<Integer, Integer>();
						p = i;
						
					}
					Iterator<String> coIt = wordums.iterator();

					//System.out.println(nab.get(4) + "index");
					while(coIt.hasNext())
					{
						
						String hold = coIt.next();
						Integer q = wordList.get(hold);
						
						if (!(q == null  || q.equals(p)))		
						{
							Integer qp = nab.get(q).get(p);
							if (qp == null)
							{	
>>>>>>> 4123e7ded942d6d23650f90d752f4d3930a5d3a8

								if (m == null)
									m = nab.get(p);
								if (m.containsKey(q))
<<<<<<< HEAD
									m.put(q, (short) (m.get(q) + 1));
								else
									m.put(q, (short) 1);

							}

						}

					}

					nab.put(p, m);

=======
									m.put(q, m.get(q) + 1);
								else
									m.put(q, 1);
								
							}
							
						}
						
					}
					
					nab.put(p, m);
				
					
>>>>>>> 4123e7ded942d6d23650f90d752f4d3930a5d3a8
				}

			}
		}
		N = s;
		System.out.println("Corpus statistic calculated.");
<<<<<<< HEAD

		listWord = new TreeMap<Short, String>();
		Iterator<Entry<String, Short>> eit = wordList.entrySet().iterator();
		while (eit.hasNext()) {
			Entry<String, Short> hold = eit.next();
			listWord.put(hold.getValue(), hold.getKey());
		}
		// uncomment to error check
		/*Iterator<Short> nit = na.keySet().iterator();
		while (nit.hasNext()) {

			short a = nit.next();
			if (listWord.get(a).equals("cacm")
					|| listWord.get(a).equals("computer")) {
				Iterator<Short> pit = nab.get(a).keySet().iterator();
				while (pit.hasNext()) {
					short b = pit.next();
					if (na.get(b) > 300)
						System.out.println(listWord.get(a) + " at " + na.get(a)
								+ " : " + listWord.get(b) + " " + na.get(b)
								+ " : at " + nab.get(a).get(b) + " : "
								+ nab.get(b).get(a));
				}
			}

		}*/

	}

	/**
	 * Pretty much standard
	 */
	public boolean equals(Object other) {
		boolean equals = false;
		if (other.getClass().equals(this.getClass())) {
			CollectCall o = (CollectCall) other;
			if (o.na.equals(this.na) && o.nab.equals(this.nab)
					&& o.wordList.equals(this.wordList) && o.N == N)
				equals = true;
		}
		return equals;
	}

	/**
	 * Used when clustering entire corpus (speed issues)
	 * 
	 * @param a
	 *            Short rep of word 1
	 * @param b
	 *            Short rep of word 2
	 * @param tm
	 *            treemap to look up with
	 * @return Dice coEf of the two terms
	 */
	private Double getDice(Short a, Short b,
			TreeMap<Short, TreeMap<Short, Double>> tm) {
		Double d = tm.get(a).get(b);
		return d;
	}

	/**
	 * 
	 * @param short rep. of word to get na of
	 * @return short na of word, or -1 if Short does not rep. term in collection
	 */
	public short getNa(short p) {
		if (na.containsKey(p))
			p = na.get(p);
		else
			p = -1; // error -- p mustbe a term in corpus.
		return p;
	}

	/**
	 * 
	 * @param word
	 *            to calulate Na of
	 * @return na of word (0 if word is not in collection)
	 */
	public short getNa(String word) {
		Short p = wordList.get(word);
		if (p != null)
			p = na.get(p);
		else
			p = 0;

		return p;
	}

	/**
	 * 
	 * @param Short
	 *            rep of word 1
	 * @param Short
	 *            rep of word 2
	 * @return nab between 2 words, or null if one of the Shorts does not map to
	 *         a term in the collection
	 */
	public Short getNab(Short p, Short q) {
		Short pq = null;
		if (p != null && q != null) {
			pq = nab.get(p).get(q);
			if (pq == null)
				pq = nab.get(q).get(p);
		}
		// uncomment for error checking
		// if (pq != null && pq > 5)
		// System.out.println("nab: " + pq);
		// if (pq == null)
		// pq = 0;

		return pq;
	}

	/**
	 * 
	 * @param word1
	 *            to calculate nab between
	 * @param word2
	 *            to calculate nab between
	 * @return nab, will calculate nab at 0 if one of the words is not in the
	 *         collection
	 */
	public Short getNab(String word1, String word2) {

		Short p = wordList.get(word1);
		Short q = wordList.get(word2);
		Short pq = null;
		if (p != null && q != null && (pq = nab.get(p).get(q)) != null)
			;
		else if (p != null && q != null && (pq = nab.get(q).get(p)) != null)
			;

		if (pq == null)
			pq = 0;
		return pq;
	}

	public Set<String> getUniqueWords() {
		return wordList.keySet();
	}

	/**
	 * 
	 * @param Buffered
	 *            reader attached to a document
	 * @return Set of terms in document Note: uses ThisAnalyzer to parse
	 *         document Change ThisAnylizer to change how document is parsed
	 *         (ex: to add stopWords)
	 */
	public Set<String> getWords(BufferedReader br) {
		Set<String> al = new HashSet<String>(30);

		try {
			TokenStream ts = sa.tokenStream("", br); // get token stream
			CharTermAttribute charTermAttribute = ts
					.addAttribute(CharTermAttribute.class);
			ts.reset();
			while (ts.incrementToken()) {
				String q = charTermAttribute.toString();
				if (!(q.indexOf("ca") == 0 && q.replaceAll("(\\d)", "")
						.length() <= q.length() - 6))
					al.add(q);
				// else
				// System.out.println(q + " " + q.length() + " "+
				// q.replaceAll("(\\d)", "") +q.replaceAll("(\\d)", "").length()
				// + (q.replaceAll("(\\d)", "").length() <= q.length() - 6));

			}

=======
		
		listWord = new TreeMap<Integer, String>();
		Iterator<Entry<String, Integer>> eit = wordList.entrySet().iterator();
		while(eit.hasNext())
		{
			Entry<String, Integer> hold = eit.next();
			listWord.put(hold.getValue(), hold.getKey());
		}
		//uncomment to error check
		/*Iterator<Integer> nit = na.keySet().iterator();
		while(nit.hasNext())
		{
			
			int a = nit.next();
			if (na.get(a) > 300)
			{
				Iterator<Integer> pit = nab.get(a).keySet().iterator();
				while(pit.hasNext())
				{int b = pit.next();
				if (na.get(b) > 300)
					System.out.println(bi.get(a) + ", " + na.get(a)+  " : " + bi.get(b) + na.get(b)  + " : at " + nab.get(a).get(b) + " : " + nab.get(b).get(a));
				}
			}
				
			
		}*/
		
	}
	//init from disc
	//returns true if successful
/**
 * 	
 * @return false if unsuccessful
 * @throws IOException if unsuccessful (file not found or corrupted)
 */
	private boolean readFromDisc() throws IOException
	{
		boolean sucess = false;
		
			new File("stats_dir");
			System.out.println("Reading nab...");
			BufferedReader br = new BufferedReader(new FileReader("stats_dir/nab.corpus"));
			nab = new TreeMap<Integer, TreeMap<Integer, Integer>>();
			int p = Integer.parseInt(br.readLine());
			while(p != 0)
			{
				TreeMap<Integer, Integer> m = new TreeMap<Integer, Integer>();
				
				int q = Integer.parseInt(br.readLine());
				while (q != 0)
				{
					int y = Integer.parseInt(br.readLine());
					//System.out.println(" :p " + p + " :y " + y + " :q " + q );
					m.put(q, y);
					
						//System.out.println(y);
					q = Integer.parseInt(br.readLine());
				}
				
				nab.put(p,m);
				p = Integer.parseInt(br.readLine());
			}
			br.close();
			System.out.println("Reading na...");
			na = new TreeMap<Integer, Integer>();
			br = new BufferedReader(new FileReader("stats_dir/na.corpus"));
			
			p = Integer.parseInt(br.readLine());
			while (p != -1)
			{
				na.put(p, Integer.parseInt(br.readLine()));
				p = Integer.parseInt(br.readLine());
				//System.out.println(p);
			}
			
			br.close();
			System.out.println("Reading word list...");
			listWord = new TreeMap<Integer, String>();
			wordList = new TreeMap<String, Integer>();
			br = new BufferedReader(new FileReader("stats_dir/listWord.txt"));
			N = Integer.parseInt(br.readLine());
			p = Integer.parseInt(br.readLine());
			
			while (p != -1)
			{
				String s = br.readLine();
				
				
				listWord.put(p, s);
				
					//System.out.println(s + " " + p);
				wordList.put(s, p);
				p = Integer.parseInt(br.readLine());
				
			}
			br.close();
			sucess = true;
			
	
		return sucess;
	}

/**
 * Method to save collection to disc, for fast initialization later
 * @return true if successful
 */
	public boolean save()
	{
		boolean sucess = false;
		try{
			//make parent directory
			new File("stats_dir").mkdir();
			//save nab
			System.out.println("Writing nab...");
			BufferedWriter bw = new BufferedWriter(new FileWriter("stats_dir/nab.corpus"));
			for (Entry<Integer, TreeMap<Integer, Integer>> e: nab.entrySet())
			{
				
				bw.write(e.getKey().intValue() + "\n");
				for(Entry<Integer, Integer> e1 : e.getValue().entrySet())
				{
					bw.write(e1.getKey().intValue() + "\n");
					bw.write(e1.getValue().intValue() + "\n");
				}
				bw.write(0 + "\n"); //terminate set
			}
			bw.write(0 + "\n");
			bw.close();
			//save na
			System.out.println("Writing na...");
			bw = new BufferedWriter(new FileWriter("stats_dir/na.corpus"));
			for(Entry<Integer, Integer> e: na.entrySet())
			{
				bw.write(e.getKey() + "\n");
				bw.write(e.getValue() + "\n");
			}
			bw.write(-1 + "\n");
			bw.close();
			System.out.println("Writing word list...");
			bw = new BufferedWriter(new FileWriter("stats_dir/listWord.txt"));
			bw.write(N + "\n");
			
			//save listword
			for (Entry<Integer, String> e: listWord.entrySet())
			{
				bw.write(e.getKey() + "\n");
				bw.write(e.getValue() + "\n");
			
			}
			bw.write(-1 + "\n");
			bw.close();
			sucess = true;
		}
		catch (Exception e)
		{System.out.println("Failure to save Collect Call Instance: " + this.toString());}
		return sucess;
	}
	/**
	 * 
	 * @param word to get int rep. of
	 * @return int representation of ord, or -1 if the word is not in the collection
	 */
	public int wordToInt( String word)
	{
		int i = -1;
		if (wordList.containsKey(word))
			i = wordList.get(word);
		return i;
	}
	
/**
 * 
 * @param Integer representation of term
 * @return term as string if integer is found, or null
 */
	public String intToWord( Integer wordIndex)
	{
		String s = null;
		if (wordIndex != null && listWord.containsKey(wordIndex))
			s = listWord.get(wordIndex);
		return s;
	}
	/**
	 * 
	 * @param Buffered reader attached to a document
	 * @return Set of terms in document
	 * Note: uses ThisAnalyzer to parse document
	 * Change ThisAnylizer to change how document is parsed (ex: to add stopWords)
	 */
	private  Set<String> getWords(BufferedReader br){
		Set<String> al = new HashSet<String>(30);
        
         try {
	        TokenStream ts = sa.tokenStream("" ,br);	//get token stream
	        CharTermAttribute charTermAttribute = ts.addAttribute(CharTermAttribute.class);
	        ts.reset();
	        
	        while (ts.incrementToken())
	        	al.add(charTermAttribute.toString());
	   
>>>>>>> 4123e7ded942d6d23650f90d752f4d3930a5d3a8
		} catch (IOException e) {

			e.printStackTrace();
		}
		return al;
	}
<<<<<<< HEAD

	/**
	 * 
	 * @param Short
	 *            representation of term
	 * @return term as string if Short is found, or null
	 */
	public String intToWord(Short wordIndex) {
		String s = null;
		if (wordIndex != null && listWord.containsKey(wordIndex))
			s = listWord.get(wordIndex);
		return s;
	}

	/**
	 * Entire collection Dice clustering at vairious levels RUN FROM COMMAND
	 * LINE LEAVE APPROX 5 HOURS ON THE IMAC writes to
	 * entire_collection_cluster_reports/DICE/...
	 * 
	 * @throws IOException
	 */
	public void marathon(double max, double inc, Operator.Measure m)
			throws IOException {

		double top = .4;
		new File("entire_collection_cluster_reports/"
				+ Operator.measureToString(m) + "/for_Excell").mkdirs();
		TreeMap<Short, TreeMap<Short, Double>> tm = treeOfMeasure(m);
		int groupNumb = 0;
		int lastGroupnumb = listWord.size();

		BufferedWriter groups = new BufferedWriter(new FileWriter(
				"entire_collection_cluster_reports/"
						+ Operator.measureToString(m)
						+ "/for_Excell/groups.txt"));
		groups.write("");
		groups.close();

		BufferedWriter thresholds = new BufferedWriter(new FileWriter(
				"entire_collection_cluster_reports/"
						+ Operator.measureToString(m)
						+ "/for_Excell/thresh.txt"));
		thresholds.write("");
		thresholds.close();

		BufferedWriter bw;
		for (int i = 0; i < 10; i++) {
			bw = new BufferedWriter(new FileWriter(
					"entire_collection_cluster_reports/"
							+ Operator.measureToString(m) + "/for_Excell/"
							+ (1 + i) + "thLargestGroup.txt", false));
			bw.write("");
			bw.close();
		}

		while (groupNumb != 1 && top > 0) {

			double degrade;

			if (lastGroupnumb == groupNumb)
				degrade = inc * 2;
			else
				degrade = inc / 2.0;
			top -= degrade;
			lastGroupnumb = groupNumb;
			groupNumb = runAround(top, tm, m);
			groups = new BufferedWriter(new FileWriter(
					"entire_collection_cluster_reports/"
							+ Operator.measureToString(m)
							+ "/for_Excell/groups.txt", true));
			thresholds = new BufferedWriter(new FileWriter(
					"entire_collection_cluster_reports/"
							+ Operator.measureToString(m)
							+ "/for_Excell/thresh.txt", true));
			groups.append(groupNumb + "\n");
			thresholds.append(top + "\n");
			groups.close();
			thresholds.close();

		}

	}
	/**
	 * 
	 * @param a Word 1 short rep
	 * @param b Word 2 sort rep
	 * @param meas Measurement wanted
	 * @return Double score, or 0 if not in collection)
	 */
	public Double measure(Short a, Short b, Operator.Measure meas) {
		Double d = null;
		Short na_a = na.get(a);
		Short na_b = na.get(b);
		Short nab_ab = getNab(a, b);
		if (nab_ab == null)
			nab_ab = 0;
		switch (meas) {
		case CHI:
			d = N
					* ((Math.pow((double) (nab_ab - 1.0 / (double) N * na_a
							* na_b), 2) / ((double) (na_a * na_b))));
			break;
		case EMI:
			d = (nab_ab / (double) N * Math.log(N * nab_ab
					/ (double) (na_a * na_b)));
			break;
		case MI:
			d = (Math.log(N * nab_ab / (double) (na_a * na_b)));
			break;
		case DICE:
			d = 2 * nab_ab / (double) (na_a + na_b);
			break;

		}
		// if (nab_ab > 300)
		// System.out.println("na = " + na_a + "nb = " + na_b + "nab = " +
		// nab_ab + " d = " + d);
		return d;
	}

	private Double measure(Short na_a, Short na_b, Short nab_ab,
			Operator.Measure meas) {
		Double d = null;

		if (nab_ab == null)
			nab_ab = 0;
		switch (meas) {
		case CHI:
			d = N
					* ((Math.pow((double) (nab_ab - 1.0 / (double) N * na_a
							* na_b), 2) / ((double) (na_a * na_b))));
			break;
		case EMI:
			d = (nab_ab / (double) N * Math.log(N * nab_ab
					/ (double) (na_a * na_b)));
			break;
		case MI:
			d = (Math.log(N * nab_ab / (double) (na_a * na_b)));
			break;
		case DICE:
			d = 2 * nab_ab / (double) (na_a + na_b);
			break;

		}
		// if (nab_ab > 300)
		// System.out.println("na = " + na_a + "nb = " + na_b + "nab = " +
		// nab_ab + " d = " + d);
		return d;
	}

	/**
	 * 
	 * @param term
	 *            to calculate terms in relation to
	 * @return TreeMap of terms in relation to original term, mapped to term's
	 *         score against original term, and sorted by value
	 */
	public TreeMap<String, Double> measure(String word, Operator.Measure m) {

		HashMap<String, Double> measure = new HashMap<String, Double>();
		Iterator<Short> words = na.keySet().iterator();
		word = word.toLowerCase();
		Short p = wordList.get(word);

		if (p != null) {
			// System.out.println( na.get(wordList.get("computer")) + " : " +
			// na.get(wordList.get("cacm")) + " : " +
			// getNab(wordList.get("computer"), wordList.get("cacm"))+ " : " +
			// getChi(wordList.get("computer"), wordList.get("cacm")));
			while (words.hasNext()) {
				Short q = words.next();

				Double d = null;
				switch (m) {
				case CHI:
					d = measure(p, q, Operator.Measure.CHI);
					break;
				case EMI:
					d = measure(p, q, Operator.Measure.EMI);
					break;
				case MI:
					d = measure(p, q, Operator.Measure.MI);
					break;
				case DICE:
					d = measure(p, q, Operator.Measure.DICE);
					break;
				}
				if (!p.equals(q) && (d > 0))
					measure.put(listWord.get(q), d);

			}
		}

		TreeMap<String, Double> mSorted = new TreeMap<String, Double>(
				new ValueComparatorSD(measure));
		mSorted.putAll(measure);
		return mSorted;

	}

	/**
	 * 
	 * @param term
	 *            to calculate terms in relation to
	 * @return TreeMap of terms in relation to original term, mapped to term's
	 *         score against original term, and sorted by value
	 */

	private TreeMap<Short, Double> measure(Short word, Operator.Measure m) {

		TreeMap<Short, Double> measure = new TreeMap<Short, Double>();
		Iterator<Short> words = na.keySet().iterator();

		// System.out.println("Calculating DICE...");

		short na_a = na.get(word);
		while (words.hasNext()) {
			Short q = words.next();
			Short na_b = na.get(q);
			Short Nab = getNab(word, q);
			if (!word.equals(q) && Nab != null) {

				// if (nb > 300)
				// System.out.println("N " + N + " nab: " + w_nab + " :na " +
				// word_na + " q " + forTeWords.get(q) + " :nb "+ nb + " MI " +
				// w_nab/(double)(word_na + nb));

				measure.put(q, measure(na_a, na_b, Nab, m));
			}
		}

		return measure;

	}

	// init from disc
	// returns true if successful
	/**
	 * 
	 * @return false if unsuccessful
	 * @throws IOException
	 *             if unsuccessful (file not found or corrupted)
	 */
	private boolean readFromDisc() throws IOException {
		boolean sucess = false;

		new File("stats_dir");
		System.out.println("Reading nab...");
		BufferedReader br = new BufferedReader(new FileReader(
				"stats_dir/nab.corpus"));
		nab = new TreeMap<Short, TreeMap<Short, Short>>();
		short p = Short.parseShort(br.readLine());
		while (p != 0) {
			TreeMap<Short, Short> m = new TreeMap<Short, Short>();

			short q = Short.parseShort(br.readLine());
			while (q != 0) {
				short y = Short.parseShort(br.readLine());
				// System.out.println(" :p " + p + " :y " + y + " :q " + q );
				m.put(q, y);

				// System.out.println(y);
				q = Short.parseShort(br.readLine());
			}

			nab.put(p, m);
			p = Short.parseShort(br.readLine());
		}
		br.close();
		System.out.println("Reading na...");
		na = new TreeMap<Short, Short>();
		br = new BufferedReader(new FileReader("stats_dir/na.corpus"));

		p = Short.parseShort(br.readLine());
		while (p != -1) {
			na.put(p, Short.parseShort(br.readLine()));
			p = Short.parseShort(br.readLine());
			// System.out.println(p);
		}

		br.close();
		System.out.println("Reading word list...");
		listWord = new TreeMap<Short, String>();
		wordList = new TreeMap<String, Short>();
		br = new BufferedReader(new FileReader("stats_dir/listWord.txt"));
		N = Short.parseShort(br.readLine());
		p = Short.parseShort(br.readLine());

		while (p != -1) {
			String s = br.readLine();

			listWord.put(p, s);

			// System.out.println(s + " " + p);
			wordList.put(s, p);
			p = Short.parseShort(br.readLine());

		}
		br.close();
		sucess = true;

		return sucess;
	}

	/**
	 * Clusters at a given threshold, records clusters to disc
	 * @param t threshold
	 * @param tm treemap of scores
	 * @param m measure being used (only needed for output)
	 * @return # of clusters
	 * @throws IOException fileWriting errors
	 */
private int runAround(Double t, TreeMap<Short, TreeMap<Short, Double>> tm,
			Operator.Measure m) throws IOException {
		ArrayList<ArrayList<Short>> al = new ArrayList<ArrayList<Short>>();
		int k = 0;
		int l = wordList.size() / 100;
		for (Short i : listWord.keySet()) {
			ArrayList<Short> hold = new ArrayList<Short>();
			ArrayList<ArrayList<Short>> trash = new ArrayList<ArrayList<Short>>();
			hold.add(i);

			for (ArrayList<Short> a : al) {
				Iterator<Short> it = a.iterator();
				boolean match = false;
				while (it.hasNext() && !match) {
					Short p = it.next();
					Double d = getDice(p, i, tm);
					// if (d != null && d > .4)
					// System.out.println(listWord.get(i) +" and " +
					// listWord.get(p) + " at " + d);
					if (d != null && d > t)
						match = true;
				}
				if (match) {
					hold.addAll(a);
					trash.add(a);
				}

			}
			al.add(hold);
			for (ArrayList<Short> u : trash) {
				al.remove(u);
			}
			if (++k % l == 0)
				System.out.println("Processing: " + k / l + "% at threshold "
						+ t + " " + Operator.measureToString(m));
		}

		BufferedWriter bw = new BufferedWriter(new FileWriter(
				"entire_collection_cluster_reports/"
						+ Operator.measureToString(m) + "/LongReportThrsh" + t
						+ ".txt"));
		bw.write("Measure: " + Operator.measureToString(m) + "\n");
		bw.write("Threshold: " + t + "\n");
		bw.write("Groups: " + al.size() + "\n");
		short y = 1;
		int[] a = new int[10];

		for (ArrayList<Short> r : al) {
			bw.write("\nGroup: " + y++ + ":");
			for (Short i : r)
				bw.write(" [" + listWord.get(i) + "]");
			int p = r.size();
			for (int i = 0; i < a.length; i++) {
				if (p > a[i]) {
					int h = a[i];
					a[i] = p;
					p = h;

				}
			}

		}
		bw.close();
		for (int i = 0; i < a.length; i++) {
			bw = new BufferedWriter(new FileWriter("/entire_collection_cluster_reports/"
					+ Operator.measureToString(m) + "/for_Excel" + (1 + i)
					+ "thLargestGroup.txt", true));
			bw.append(a[i] + "\n");
			bw.close();
		}

		return al.size();

	}

	/**
	 * Method to save collection to disc, for fast initialization later
	 * 
	 * @return true if successful
	 */
	public boolean save() {
		boolean sucess = false;
		try {
			// make parent directory
			new File("stats_dir").mkdir();
			// save nab
			System.out.println("Writing nab...");
			BufferedWriter bw = new BufferedWriter(new FileWriter(
					"stats_dir/nab.corpus"));
			for (Entry<Short, TreeMap<Short, Short>> e : nab.entrySet()) {

				bw.write(e.getKey().intValue() + "\n");
				for (Entry<Short, Short> e1 : e.getValue().entrySet()) {
					bw.write(e1.getKey().intValue() + "\n");
					bw.write(e1.getValue().intValue() + "\n");
				}
				bw.write(0 + "\n"); // terminate set
			}
			bw.write(0 + "\n");
			bw.close();
			// save na
			System.out.println("Writing na...");
			bw = new BufferedWriter(new FileWriter("stats_dir/na.corpus"));
			for (Entry<Short, Short> e : na.entrySet()) {
				bw.write(e.getKey() + "\n");
				bw.write(e.getValue() + "\n");
			}
			bw.write(-1 + "\n");
			bw.close();
			System.out.println("Writing word list...");
			bw = new BufferedWriter(new FileWriter("stats_dir/listWord.txt"));
			bw.write(N + "\n");

			// save listword
			for (Entry<Short, String> e : listWord.entrySet()) {
				bw.write(e.getKey() + "\n");
				bw.write(e.getValue() + "\n");

			}
			bw.write(-1 + "\n");
			bw.close();
			sucess = true;
		} catch (Exception e) {
			System.out.println("Failure to save Collect Call Instance: "
					+ this.toString());
		}
		return sucess;
	}
	/**
	 * 
	 * @param term
	 * @param T threshold
	 * @param M measure
	 * @return an Arraylist of words that correspond to the given threshold in the collection, and the term itself
	 */
	public ArrayList<String> timeLimit(String term, double T, Operator.Measure M) {
		ArrayList<String> al = new ArrayList<String>();
		al.add(term);
		TreeMap<String, Double> tm = measure(term, M);

		if (wordToInt(term) > 0)// If term is in collection
=======
/**
 * 	
 * @param word1 to calculate nab between
 * @param word2 to calculate nab between
 * @return nab, will calculate nab at 0 if one of the words is not in the collection
 */
public Integer getNab(String word1, String word2)
	{
		
		Integer p = wordList.get(word1);
		Integer q = wordList.get(word2);
		Integer pq = null;
		if (p != null && q != null && (pq = nab.get(p).get(q)) != null);
		else if  (p != null && q != null && (pq = nab.get(q).get(p)) != null);
	
		if (pq == null)
			pq = 0;
		return pq;
	}
/**
 * 
 * @param word to calulate Na of
 * @return na of word (0 if word is not in collection)
 */
	public int getNa(String word)
	{
		Integer p = wordList.get(word);
		if (p != null)
			p = na.get(p);
		else 
			p = 0;
	
		return p;
	}
	/**
	 * 
	 * @param int rep. of word to get na of
	 * @return int na of word, or -1 if integer does not rep. term in collection
	 */
	public int getNa(int p)
	{
		if (na.containsKey(p))
			p = na.get(p);
		else 
			p = -1; //error -- p mustbe a term in corpus.
		return p;
	}
	/**
	 * 
	 * @param Integer rep of word 1
	 * @param Integer rep of word 2
	 * @return nab between 2 words, or null if one of the integers does not map to a term in the collection
	 */
public  Integer getNab(Integer p, Integer q)
	{
		Integer pq = null;
		if (p != null && q != null)
			if ((pq = nab.get(p).get(q)) == null);
				pq = nab.get(q).get(p);
		//uncomment for error checking
		//if (pq != null && pq > 5)
			//System.out.println("nab: " + pq);	
		//if (pq == null)
			//pq = 0;

		return pq;
	}
	/**
	 * 
	 * @param term to calculate terms in relation to
	 * @return TreeMap of terms in relation to original term, mapped to term's score against original term, and sorted by value
	 */
	public TreeMap<String, Double> measureMI(String word)
	{
		
		HashMap<String, Double> measureMI= new HashMap<String, Double>();
		Iterator<Integer> words = na.keySet().iterator();
		word = word.toLowerCase();
		Integer p = wordList.get(word);
		System.out.println("Calculating MIM...");
		if (p != null)
		{
			Integer word_na = na.get(p);
			
			while(words.hasNext())
			{
				Integer q = words.next();
				Integer w_nab = null;
				if (!p.equals(q) && (w_nab = getNab( p, q)) != null)
				{
					
					Integer nb = na.get(q);
					//if (nb > 50)
						//System.out.println("nab: " + w_nab + " :na " + word_na + " q " + listWord.get(q) + " :nb "+ nb + " MI " + Math.log(N*w_nab/(double)(word_na*nb)));	
					
					measureMI.put(listWord.get(q), Math.log(N*w_nab/(double)(word_na*nb)));		
				}
			}
		}

		TreeMap<String, Double> miSorted = new TreeMap<String, Double>(new ValueComparatorSD(measureMI));
		miSorted.putAll(measureMI);
		return miSorted;
		
	        }
	/**
	 * 
	 * @param term to calculate terms in relation to
	 * @return TreeMap of terms in relation to original term, mapped to term's score against original term, and sorted by value
	 */
	public TreeMap<String, Double> measureEMI(String word)
{
	
	HashMap<String, Double> measure = new HashMap<String, Double>();
	Iterator<Integer> words = na.keySet().iterator();
	word = word.toLowerCase();
	Integer p = wordList.get(word);
	System.out.println("Calculating EMIM...");
	if (p != null)
	{
		Integer word_na = na.get(p);
		while(words.hasNext())
		{
			Integer q = words.next();
			Integer w_nab = null;
			if (!p.equals(q) && (w_nab = getNab( p, q)) != null)
			{
				
				Integer nb = na.get(q);
				//if (nb > 50)
					//System.out.println("N " + N + " nab: " + w_nab + " :na " + word_na + " q " + listWord.get(q) + " :nb "+ nb + " chi " + Math.pow(w_nab-word_na*nb/(double)N, 2)/(double)(word_na*nb/(double)N));
				
				measure.put(listWord.get(q), Math.pow(w_nab-word_na*nb/(double)N, 2)/(double)(word_na*nb/(double)N));
						//w_nab/N*Math.log(N*w_nab.intValue()/(double)(word_na.intValue()*nb.intValue())));		
			}
		}
	}

	TreeMap<String, Double> mSorted = new TreeMap<String, Double>(new ValueComparatorSD(measure));
	mSorted.putAll(measure);
	return mSorted;
	
        }
	/**
	 * 
	 * @param term to calculate terms in relation to
	 * @return TreeMap of terms in relation to original term, mapped to term's score against original term, and sorted by value
	 */
	public TreeMap<String, Double> measureDice(String word)
{
	
	HashMap<String, Double> measure = new HashMap<String, Double>();
	Iterator<Integer> words = na.keySet().iterator();
	word = word.toLowerCase();
	Integer p = wordList.get(word);
	System.out.println("Calculating DICE...");
	if (p != null)
	{
		Integer word_na = na.get(p);
		while(words.hasNext())
		{
			Integer q = words.next();
			Integer w_nab = null;
			if (!p.equals(q) && (w_nab = getNab( p, q)) != null)
			{
				
				Integer nb = na.get(q);
				//if (nb > 300)
					//System.out.println("N " + N + " nab: " + w_nab + " :na " + word_na + " q " + forTeWords.get(q) + " :nb "+ nb + " MI " + w_nab/(double)(word_na + nb));	
				
				measure.put(listWord.get(q), 2*w_nab/(double)(word_na + nb));		
			}
		}
	}

	TreeMap<String, Double> mSorted = new TreeMap<String, Double>(new ValueComparatorSD(measure));
	mSorted.putAll(measure);
	return mSorted;
	
        }
	/**
	 * 
	 * @param term to calculate terms in relation to
	 * @return TreeMap of terms in relation to original term, mapped to term's score against original term, and sorted by value
	 */
	public TreeMap<String, Double> measureChi(String word)
{
	
	HashMap<String, Double> measure = new HashMap<String, Double>();
	Iterator<Integer> words = na.keySet().iterator();
	word = word.toLowerCase();
	Integer p = wordList.get(word);
	System.out.println("Calculating CHI...");
	if (p != null)
	{
		Integer word_na = na.get(p);
		while(words.hasNext())
		{
			Integer q = words.next();
			Integer w_nab = null;
			if (!p.equals(q) && (w_nab = getNab( p, q)) != null)
			{
				
				Integer nb = na.get(q);
				//if (nb > 300)
					//System.out.println(":N " + N + " :nab " + w_nab + " :na " + word_na + " q " + forTeWords.get(q) + " :nb "+ nb + " MI " + Math.pow((w_nab- 1/(double)N*word_na*nb), 2)/(double)(word_na*nb));	
				
				measure.put(listWord.get(q), Math.pow((w_nab- 1/(double)N*word_na*nb), 2)/(double)(word_na*nb));		
			}
		}
	}

	TreeMap<String, Double> mSorted = new TreeMap<String, Double>(new ValueComparatorSD(measure));
	mSorted.putAll(measure);
	return mSorted;
	
        }

	public boolean equals(Object other)
{
	boolean equals = false;
	if (other.getClass().equals(this.getClass()))
	{
		CollectCall o = (CollectCall)other;
		if (o.na.equals(this.na) && o.nab.equals(this.nab)&& o.wordList.equals(this.wordList)&&o.N == N )
			equals = true;
	}
	return equals;
}

	public String toString()
{
	return "CollectCalling: " + wordList.size() + " terms and " + N + " documents";
}

	/**
	 * 
	 * @param term Term to expand (stem)
	 * @param T threshold
	 * @return will return terms in collection with a dice coefficient above T with the term argument
	 */
	public ArrayList<String> timeLimitDice(String term, double T)
	{
		ArrayList<String> al = new ArrayList<String>();
		TreeMap<String, Double> tm = measureDice(term);
		
		if (wordToInt(term) > 0)//If term is in collection
		{
			Iterator<String> cc = tm.keySet().iterator();
			Iterator<Double> dd = tm.values().iterator();
			while (dd.next() > T)
				al.add(cc.next());
		
		}
		return al;
	}
	/**
	 * 
	 * @param term Term to expand (stem)
	 * @param T threshold
	 * @return will return terms in collection with a dice coefficient above T with the term argument
	 */
	public ArrayList<String> timeLimitEMI(String term, double T)
	{
		ArrayList<String> al = new ArrayList<String>();
		TreeMap<String, Double> tm = measureEMI(term);
		
		if (wordToInt(term) > 0)//If term is in collection
>>>>>>> 4123e7ded942d6d23650f90d752f4d3930a5d3a8
		{
			Iterator<String> cc = tm.keySet().iterator();
			Iterator<Double> dd = tm.values().iterator();
			while (dd.next() > T)
				al.add(cc.next());
<<<<<<< HEAD

=======
		
>>>>>>> 4123e7ded942d6d23650f90d752f4d3930a5d3a8
		}
		return al;
	}
	/**
<<<<<<< HEAD
	 * reports the # of unique terms in the collection, and the number of documents
	 */
	public String toString() {
		return "CollectCalling: " + wordList.size() + " terms and " + N
				+ " documents";
	}
	/**
	 * 
	 * @param m Measurement type
	 * @return TreeMap, for marathon(...)
	 */
	private TreeMap<Short, TreeMap<Short, Double>> treeOfMeasure(
			Operator.Measure m) {
		System.out.println("Calculating " + Operator.measureToString(m)
				+ " Measure for collection...");
		int n = 0;
		int l = wordList.size() / 100;
		TreeMap<Short, TreeMap<Short, Double>> tm = new TreeMap<Short, TreeMap<Short, Double>>();
		for (short i : listWord.keySet()) {
			tm.put(i, measure(i, m));
			if (++n % l == 0)
				System.out.println(n / l + "% of Collection's "
						+ Operator.measureToString(m)
						+ " Measurements calculated");
		}
		return tm;
	}

	/**
	 * 
	 * @param word
	 *            to get short rep. of
	 * @return short representation of ord, or -1 if the word is not in the
	 *         collection
	 */
	public short wordToInt(String word) {
		short i = -1;
		if (wordList.containsKey(word))
			i = wordList.get(word);
		return i;
	}
=======
	 * 
	 * @param term Term to expand (stem)
	 * @param T threshold
	 * @return will return terms in collection with a dice coefficient above T with the term argument
	 */
	public ArrayList<String> timeLimitMI(String term, double T)
	{
		ArrayList<String> al = new ArrayList<String>();
		TreeMap<String, Double> tm = measureMI(term);
		
		if (wordToInt(term) > 0)//If term is in collection
		{
			Iterator<String> cc = tm.keySet().iterator();
			Iterator<Double> dd = tm.values().iterator();
			while (dd.next() > T)
				al.add(cc.next());
		
		}
		return al;
	}
	/**
	 * 
	 * @param term Term to expand (stem)
	 * @param T threshold
	 * @return will return terms in collection with a dice coefficient above T with the term argument
	 */
	public ArrayList<String> timeLimitChi(String term, double T)
	{
		ArrayList<String> al = new ArrayList<String>();
		TreeMap<String, Double> tm = measureChi(term);
		
		if (wordToInt(term) > 0)//If term is in collection
		{
			Iterator<String> cc = tm.keySet().iterator();
			Iterator<Double> dd = tm.values().iterator();
			while (dd.next() > T)
				al.add(cc.next());
		
		}
		return al;
	}
	
>>>>>>> 4123e7ded942d6d23650f90d752f4d3930a5d3a8
}
