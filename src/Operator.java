import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

public class Operator {

	/**
	 * 
	 * @author Alice 
	 * Class to return groupings by cluster and stem
	 */

	/**
	 * Measures available
	 */
	public enum Measure {
		CHI, DICE, MI, EMI
	}

	/**
	 * 
	 * @param m
	 *            Measure
	 * @return String Rep. of M
	 */
	public static String measureToString(Measure m) {
		String d = null;
		switch (m) {
		case CHI:
			d = "CHI";
			break;
		case EMI:
			d = "EMI";
			break;
		case MI:
			d = "MI";
			break;
		case DICE:
			d = "DICE";
			break;
		}
		return d;
	}
	/**
	 * Connections at set measure and threshold
	 */
	private TreeMap<String, ArrayList<ArrayList<Short>>> keep;
	/**
	 * Collect Call Used
	 */
	private CollectCall cc;
	/**
	 * Stem Map used
	 */
	private SortedMap<String, SortedSet<String>> stems;
	/**
	 * threshold
	 */
	private double t = 1;

	/**
	 * mesure in use
	 */
	private Measure m = null;

	/**
	 * run Operator.init(Measure, double) before using
	 * 
	 * @param Collect
	 *            Call of Directory
	 * @param directory
	 *            of collection
	 * @throws Exception
	 *             (File reading errors)
	 */
	public Operator(CollectCall cc, String dir) throws Exception {
		this.cc = cc;
		stems = getStem2WordsMap(dir);

		this.t = .1;

	}

	/**
	 * 
	 * @param word
	 *            to get associations of
	 * @return words in the original stem grouping that are greater than the t
	 *         using the measure this operator was last int(...) with
	 */
	public ArrayList<String> associatedWords(String word) {
		ArrayList<String> al = new ArrayList<String>();
		al.add(word);
		Short p = cc.wordToInt(word);
		if (p != null) {
			ArrayList<ArrayList<Short>> hold = keep.get(WordCluster
					.stemTerm(word));
			for (ArrayList<Short> a : hold) {
				if (a.contains(p))
					for (Short w : a)
						if (!p.equals(w))
							al.add(cc.intToWord(w));
			}
		}
		return al;
	}

	public static SortedMap<String, SortedSet<String>> stemClusters(String stem, SortedSet<String> words, Measure m, double t, CollectCall cc) {
		ArrayList<ArrayList<Short>> al = new ArrayList<ArrayList<Short>>();
		
		for(String word: words)
		{
			ArrayList<Short> hold = new ArrayList<Short>();
			ArrayList<ArrayList<Short>> trash = new ArrayList<ArrayList<Short>>();
			Short p = cc.wordToInt(word);
			hold.add(p);
			for (ArrayList<Short> a: al)
			{
				for(Short s: a)
				{
					Double d = cc.measure(p, s, m);
					if (d > t)
					{
						hold.addAll(a);
						trash.add(a);
					}
				}
			}
			for (ArrayList<Short> tr: trash)
				al.remove(tr);
			al.add(hold);
			
		}
		SortedMap<String, SortedSet<String>> tm = new TreeMap<String, SortedSet<String>>();
		int r = 0;
		for (ArrayList<Short> a: al)
		{
			SortedSet<String> ts = new TreeSet<String>();
			for(Short s: a)
			{
				ts.add(cc.intToWord(s));
				
			}
			tm.put(stem + r, ts);
			
		}
		System.out.println(tm);
		return tm;
			
	}
	/**
	 * 
	 * @return Measure last init(...) with (or null)
	 */
	public Measure getMeasure() {
		return m;
	}

	/**
	 * 
	 * @param text_dir
	 * @return map of words to stems
	 * @throws IOException
	 *             for file reading issues
	 */
	private SortedMap<String, SortedSet<String>> getStem2WordsMap(
			String text_dir) throws IOException {
		SortedMap<String, SortedSet<String>> sortMap = new TreeMap<String, SortedSet<String>>();
		File folder = new File(text_dir);
		File[] listOfFiles = folder.listFiles();
		String filename;

		for (File file : listOfFiles) { // Going through all of the files in the
										// directory denoted by text_dir
			if (file.isFile()) {// To avoid any runtime errors for missing files
				filename = file.getName();
				/**
				 * For a given file, the next twelve lines read all of the
				 * strings in the file line by line and concatenate them into
				 * one long string to be processed in subsequent code.
				 */
				// Learned how to read the lines in files from StackOverflow
				BufferedReader br = new BufferedReader(new FileReader(text_dir
						+ "/" + filename));

				String stem = null;
				Set<String> doc = cc.getWords(br);
				for (String word : doc) {
					SortedSet<String> prev_values = new TreeSet<String>();
					stem = WordCluster.stemTerm(word);
					if (sortMap.containsKey(stem)) {
						prev_values = sortMap.get(stem);
					}
					prev_values.add(word);
					sortMap.put(stem, prev_values);
				}

			}
		}
		return sortMap;
	}

	/**
	 * 
	 * @return threshold last int(...) with
	 */
	public double getT() {
		return t;
	}

	/**
	 * Initializes for use &
	 * Writes Stats to file if specified (see stem_cluster_reports/[meas]/...)
	 * WARNING: clear files before writing 
	 * @param meas
	 *            Term Association Measure
	 * @param t
	 *            threshold of connection
	 * @param record
	 *            record to file
	 * @throws IOException
	 *             file writing errors
	 */
	public void init(Measure meas, double t, boolean record) throws IOException {
		new File("stem_cluster_reports/" + measureToString(meas)+"/for_Excel").mkdirs();
		keep = new TreeMap<String, ArrayList<ArrayList<Short>>>();
		int p = 0;
		int l = stems.size() / 100;
		int groups = 0;
		this.t = t;
		m = meas;
		for (String stem : stems.keySet()) {
			SortedSet<String> matches = stems.get(stem);
			ArrayList<Short> holdNumb = new ArrayList<Short>();

			for (String w : matches) // store matches as int
			{
				holdNumb.add(cc.wordToInt(w));
				// System.out.println(w);
			}

			ArrayList<ArrayList<Short>> clusters = new ArrayList<ArrayList<Short>>();

			for (Short w : holdNumb) {
				ArrayList<Short> thisCluster = new ArrayList<Short>();
				ArrayList<ArrayList<Short>> trash = new ArrayList<ArrayList<Short>>();
				thisCluster.add(w);
				for (ArrayList<Short> hold : clusters) {
					boolean cluster = false;
					for (Short s2 : hold) {
						// System.out.println(w +" : " + s2);
						Double val = cc.measure(w, s2, meas);
						if (val == null)
							val = 0.0;
						if (val > t) {
							cluster = true;
						}
					}
					if (cluster) {
						thisCluster.addAll(hold);
						trash.add(hold);
					}
				}
				for (ArrayList<Short> throwAway : trash) {
					clusters.remove(throwAway);
				}
				clusters.add(thisCluster);

			}
			keep.put(stem, clusters);
			groups += clusters.size();
			if (++p % (l * 10) == 0)
				System.out.println("Processing: " + p / l + "% at threshold "
						+ t);

		}
		if (record)
		{
				BufferedWriter bw = new BufferedWriter(new FileWriter(
						"stem_cluster_reports/" + measureToString(meas)+ "/LongReportThrsh" + t + ".txt"));
				bw.write(toString());
				bw.write("\nGroups: " + groups + "\n");
		
				for (Entry<String, ArrayList<ArrayList<Short>>> stemSet : keep
						.entrySet()) {
					bw.write("\nstem: [" + stemSet.getKey() + "]");
					int k = 1;
					for (ArrayList<Short> cluster : stemSet.getValue()) {
						bw.write("\n      Grouping " + k++ + ":");
						for (Short word : cluster)
							bw.write(" [" + cc.intToWord(word) + "]");
		
					}
				}
				bw.close();
				bw = new BufferedWriter(new FileWriter("stem_cluster_reports/" + measureToString(meas)+ "/for_Excel/Thresholds.txt", true));
				bw.append(t + "\n");
				bw.close();
				bw = new BufferedWriter(new FileWriter("stem_cluster_reports/" + measureToString(meas)+ "/for_Excel/Groupings.txt", true));
				bw.append(groups + "\n");
				bw.close();
		}
		

	}

	/**
	 * for testing prints all the words in the collection this probably wont fit
	 * on your screen.
	 */
	public void test() {
		for (String w : cc.getUniqueWords())
			System.out.println(w + " : " + associatedWords(w));
	}

	/**
	 * Operator Details
	 */
	@Override
	public String toString() {
		String s = "OPERATOR: ";
		if (m == null)
			s += " Not Initialized! ";
		else
			s += " Initialized at threshold t " + t + " using measurement "
					+ measureToString(m);
		s += " and " + cc.toString();
		return s;
	}
	/**
	 * Writes a huge number of values to file
	 * will overwrite all files in "stemm_cluster_reports
	 * @throws IOException for deletion and writing issues
	 */
	public void Marathon() throws IOException
	{
		Path path = FileSystems.getDefault().getPath("stem_cluster_reports");
		Files.deleteIfExists(path);
		Double t = 7.0;
		Operator.Measure m = Operator.Measure.MI;
		while (t > 0)
		{
			init(m, t, true);
			t -= .05;
		}
		m = Operator.Measure.EMI;
		t = 5.0;
		while (t > 0)
		{
			init(m, t, true);
			t -= .001;
		}
		m = Operator.Measure.CHI;
		t = 500.0;
		while (t > 0)
		{
			init(m, t, true);
			t -= 2.0;
		}
		m = Operator.Measure.DICE;
		t = 3.0;
		while (t > 0)
		{
			init(m, t, true);
			t -= .01;
		}
		
	}

}
