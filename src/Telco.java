
import java.util.ArrayList;
import java.util.Set;
import java.util.TreeMap;

public class Telco {


	/**
	 * Set of nodes
	 */
	private Set<Integer> node;
	/**
	 * Set of Integers
	 */

	private TreeMap<Integer, TreeMap<Integer, Double>> wires;
	/**
	 * Collect Call used 
	 */
	private CollectCall cc; //there is no way to modify a collect call after it is created
	/**
	 * stem nodes come from
	 */
	private String stem;
	/**
	 * 
	 * @param cc CollectCall to base calculations on
	 * @param ss Set of strings to calculate connections between
	 * @param stem stem the collections of words is represented by
	 */
	public Telco (CollectCall cc, Set<String> ss, String stem)
	{
		this.cc = cc; //unmodifiable class (safe to assign this way)
		this.stem = stem;
		for(String s: ss)
		{
			int p;
			if ((p = cc.wordToInt(s)) > -1)
				node.add(p);
			
		}
		connect();
	}
	/**
	 * calculate connections between nodes 
	 */
	public void connect()
	{
		

			wires = new TreeMap<Integer, TreeMap<Integer, Double>>();
			for (Integer n1: node)
			{
				TreeMap<Integer, Double> m = new TreeMap<Integer, Double>();
				for (Integer n2: node)
				{
					
					if (!n2.equals(n1) && !wires.containsKey(n2))
					{
					int na = cc.getNa(n1);
					int nb = cc.getNa(n2);
					int nab = cc.getNab(na, nb);
					m.put(n2, nab/(double)(na+nb));
					}
					
				}
				wires.put(n1, m);
			}
		}
	
	public boolean equals(Object o)
	{
		boolean equal = false;
		if (this.getClass().equals(o.getClass()))
		{
			Telco other = (Telco)o;
			if (this.node.equals(other.wires) && this.cc.equals(other.cc))
				equal = true;
		}
		return equal;
	}
	public String toString()
	{
		String s = "Telco: {";
		for(Integer n: node)
		{
			s += (cc.intToWord(n) + ", ");
		}
		s = s.substring(0, s.length() -1) + "} using ";
		s+= cc.toString();
		s+= " system";
		return s;
	}
	public String getStem() {
		return stem;
	}
/**
 * 
 * @return memory reference to Collect call (unmodifiable class)
 */
	public CollectCall getCollectCall()
	{
	return cc;
	}

	
}
