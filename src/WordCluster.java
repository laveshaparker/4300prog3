import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.TreeSet;

import org.tartarus.snowball.ext.PorterStemmer;

import edu.stanford.nlp.tagger.maxent.MaxentTagger;

/**
 * You may, and should, implement any additional methods
 * including a main() method for outputing results, but
 * you should still implement and make use of the following methods:
 * getStem2WordsMap()
 * subclusterStem2WordsMap()
 * getWord2StemMap()
 */
public class WordCluster {
	public static SortedMap<String, SortedSet<String>> stem2words;
	public static SortedMap<Integer, SortedSet<String>> num2stems;
	public static SortedMap<String, SortedSet<String>> morethanmax = new TreeMap<String, SortedSet<String>>();
	public static SortedMap<String, SortedSet<String>> newmorethanmax = new TreeMap<String, SortedSet<String>>();
	public static Integer maxClassSize = 9;

	// The HashMap that maps the number of elements in the stem 
	// class to the name of the stem class (the stem itself).
	// public static HashMap<Integer, String> numel; 

	public static void main(String[] arg) throws IOException{
		//the next line is just for testing
		String text_dir = "data/txt";
		stem2words = getStem2WordsMap(text_dir);
		num2stems = stems2nums(stem2words);
		System.out.println("Displaying the results of number of classes in each group" + 
				"(groups are arranged by the number of words in the stem class)\n");
		Integer numIn;
		for (Integer i : num2stems.keySet()){
			numIn = num2stems.get(i).size();
			System.out.println("In the group with " + i + " elements, there are " + numIn + " stem classes.");
		}

		System.out.println("\n");
		System.out.println("Now displaying the stems and corresponding words for"
				+ "those stems for classes with more than 10 words.\n");
		for (Integer i : num2stems.keySet()){
			if (i>=10) {
				System.out.println("\nStem classes with "+i+" members :");
				System.out.println("-----------------------------------");
				for (String stem : num2stems.get(i)){
					System.out.println("Stem : "+stem+" words : "+stem2words.get(stem).toString());
					morethanmax.put(stem,stem2words.get(stem));
				}
			}
		}
		System.out.println("Separating the large classes by part of speech");
		subclusterStem2WordsMap();
		for (String stem : newmorethanmax.keySet()){
			System.out.println("Stem : " + stem + " words : " + newmorethanmax.get(stem).toString());
		}
	}

	//////////////////////////////////////START OF STUDENT-DEFINED HELPER METHODS///////////////////////////////////////////

	/** Given a string, function stemTerm returns the 
	 *  stem associated with this term.
	 * @param term
	 * @return
	 */
	public static String stemTerm (String term){
		PorterStemmer ps = new PorterStemmer();
		ps.setCurrent(term);
		ps.stem();
		return ps.getCurrent();
	}


	/**
	 * Given a SortedMap of keys(stems) and values (words related to the stem), Returns a SortedMap<Integer, String> 
	 * such that the keys are Integers that correspond to the number of words in the stem collection denoted by the
	 * value (the String) which is the stem itself.
	 */
	public static SortedMap<Integer, SortedSet<String>> stems2nums (SortedMap<String, SortedSet<String>> stem2words){
		SortedMap<Integer, SortedSet<String>> sm = new TreeMap<Integer, SortedSet<String>>();
		Integer classSize;
		for (String stem : stem2words.keySet()){
			SortedSet<String> prev_value = new TreeSet<String>();
			classSize = stem2words.get(stem).size();
			if (sm.containsKey(classSize)){
				prev_value = sm.get(classSize);
			}
			prev_value.add(stem);
			sm.put(classSize, prev_value);
		}
		return sm;
	}


	/**
	 * Given a String representing a stem class, and a String of the form wordBelongingToClass_partofspeech,
	 * this function returns a String of the form stem_partOfSpeech.
	 */
	public static String word_pos2stem_pos (String stem, String word_pos){
		int i = word_pos.indexOf("_");
		return stem+word_pos.substring(i+1);
	}
	/////////////////////////////////////////END OF STUDENT-DEFINED HELPER METHODS///////////////////////////////////////////

	/**
	 * Given a directory containing a set of text files (CACM), return a mapping from stem to words with the given stem.
	 * @param text_dir
	 * @throws IOException 
	 */
	public static SortedMap<String, SortedSet<String>> getStem2WordsMap(String text_dir) throws IOException {
		SortedMap<String, SortedSet<String>> sortMap = new TreeMap<String, SortedSet<String>>();
		File folder = new File(text_dir);
		File[] listOfFiles = folder.listFiles();
		String filename;
		int k;
		for (File file : listOfFiles){ // Going through all of the files in the directory denoted by text_dir
			if (file.isFile()){//To avoid any runtime errors for missing files
				filename = file.getName();
				/** For a given file, the next twelve lines read all of the strings in the 
				 *  file line by line and concatenate them into one long string to be 
				 *  processed in subsequent code.
				 */
				//Learned how to read the lines in files from StackOverflow
				BufferedReader br = new BufferedReader(new FileReader(text_dir+"/"+filename));
				String file_text = "";
				try {
					String line = br.readLine();
					while (line != null) {
						file_text = file_text + " " + line;
						line = br.readLine();
					}
				} finally {
					br.close();
				}
				/**For a given file, going through the String representation of that file
				 *  and adding each word to the map by first tokenizing the string (to get
				 *  all of the words separately), finding the stem of that word using the
				 *  helper method stemTerm, and adding the word to the map with its stem
				 *  as the key. 
				 */
				StringTokenizer t = new StringTokenizer(file_text);
				String word;
				String stem = "";
				while (t.hasMoreTokens()){
					SortedSet<String> prev_values = new TreeSet<String>();
					word = t.nextToken().toLowerCase();
					stem = stemTerm(word);
					if (sortMap.containsKey(stem)){
						prev_values = sortMap.get(stem);				
					}
					prev_values.add(word);
					sortMap.put(stem, prev_values);
				}

			}
		}
		return sortMap;
	}


	/**
	 * Given a clustering of words with their stem as the key,
	 * return a new clustering of words, where each cluster is 
	 * a subcluster of a stem class, and the respective key
	 * can be something arbitrary (e.g. stem + number, such as "polic1", "polic2")
	 * 
	 */
	public static void subclusterStem2WordsMap(){
		MaxentTagger tagger = new MaxentTagger("/Users/vesha/Desktop/programming3/stanford-postagger-2013-11-12/models/english-left3words-distsim.tagger");
		String tag;
		String new_stem;
		SortedSet<String> prev_value;
		ArrayList<String> al = new ArrayList<String>();
		for (String stem : morethanmax.keySet()){
			al.add(stem);
			for (String word : morethanmax.get(stem)){
				prev_value = new TreeSet<String>();
				tag = tagger.tagString(word);
				new_stem = word_pos2stem_pos(stem, tag);
				if (newmorethanmax.containsKey(new_stem)){
					prev_value = newmorethanmax.get(new_stem);
				}
				prev_value.add(word);
				newmorethanmax.put(new_stem, prev_value);
			}
		}
	}


	/**
	 * Given a map that maps a key to a set of words,
	 * return a map that maps each word in the set to the key.
	 * e.g. {"polic":{"police","policy"}} --> {"policy":"polic", "police":"polic"}
	 * @param key2words2Map
	 */
	public static SortedMap<String, String> getWord2KeyMap(SortedMap<String, SortedSet<String>> key2wordsMap){
		SortedMap<String, String> map = new TreeMap<String, String>();
		for (String k : key2wordsMap.keySet()){
			SortedSet<String> values = key2wordsMap.get(k);
			for (String word : values){
				map.put(word, k);
			}
		}
		return map;
	}
}