import java.io.IOException;
import java.util.ArrayList;


public class ExampleFromAlice {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		//if first run
		String dir = "data/txt";
		CollectCall cc = new CollectCall(dir);
		cc.save();
		
		//if saved to disc
		//
		//cc = new CollectCall();
		
		//make operator
		Operator o = new Operator(cc, dir);
		//ALWAYS INIT BEFORE USE
		o.init(Operator.Measure.DICE, .01, false);
		
		//get stem limited shortest path clusters
		ArrayList<String> wordsLikeThis = o.associatedWords("word");
		
		//get coefficient determined limit (not refined to stem class
		ArrayList<String> otherWordsLikeThis = cc.timeLimit("word", .01, Operator.Measure.DICE);
		
		
		//NOTES
		/*
		 * This Collection has the ca...... numbers striped from it
		 * as well as using the Lucene tokenizer
		 * Some methods will throw exceptions if other words are used with them (neither of these will)
		 * to change the indexing methods alter thisAnylyzer or CollectCall.getWords(BufferedReader)
		 * and rebuild collect call
		 * 
		 * 
		 */
		
		

	}

}
